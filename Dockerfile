FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Ben Jones <ben.dylan.jones@cern.ch>

# CERN CA
ADD cerngridca.crt /etc/pki/ca-trust/source/anchors/cerngridca.crt
ADD cernroot.crt /etc/pki/ca-trust/source/anchors/cernroot.crt
RUN update-ca-trust


ADD repos/UMD-4-updates.repo /etc/yum.repos.d/
ADD repos/batch7-stable.repo /etc/yum.repos.d/
ADD repos/eos7-stable.repo /etc/yum.repos.d/
ADD repos/htcondor-stable.repo  /etc/yum.repos.d/
ADD repos/egi-trustanchors.repo /etc/yum.repos.d/

ADD http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3 /etc/pki/rpm-gpg/

RUN yum -y update
RUN yum clean all
RUN yum -y groupinstall 'Development Tools'
RUN yum -y install -y ca-policy-egi-core
RUN yum -y install \
  authconfig \
  boost-devel \
  glexec-wn \
  glibc-headers \
  globus-proxy-utils \
  globus-gass-copy-progs \
  lcmaps-plugins-condor-update \
  lcmaps-plugins-mount-under-scratch \
  lcmaps-plugins-namespace \
  redhat-lsb-core \
  sssd \
  sssd-client \
  which

RUN yum -y install \
attr cyrus-sasl-devel e2fsprogs-devel eos-fuse-core expat-devel singularity \
file-devel giflib-devel gmp-devel gpm-devel kernel-devel libacl-devel \
libattr-devel libcap-devel libcom_err-devel libcurl-devel libdrm-devel \
libdrm-devel libstdc++-devel libuuid-devel libxml2-devel lockdev-devel \
libjpeg-turbo-devel openldap-devel netpbm-devel popt-devel python-devel \
rpm-devel tcl-devel tk-devel openssh-clients PyXML HEP_OSlibs_SL6 \
voms-clients3 wlcg-voms-alice wlcg-voms-atlas wlcg-voms-lhcb wlcg-voms-cms \
wlcg-voms-ops ca-policy-egi-core emi-wn 

RUN yum -y install condor \
	condor-external-libs \
	condor-procd \
	condor-python \
	condor-classads \
	htcondorcrons \
	ngbauth-htcondor \
	ngbauth-batch \
	python-krbV

RUN yum -y install openafs-client
RUN yum -y install docker goggles

ADD condor_credmon.conf /etc/condor/
RUN mkdir -p /usr/local/condor
ADD job_wrapper /usr/local/condor/
ADD get-cgroup /usr/local/condor/
RUN mkdir -p /etc/singularity
RUN mkdir -p /etc/batch_credds
RUN mkdir -p /etc/openafs
RUN mkdir -p /usr/vice/etc
ADD CellServDB /usr/vice/etc/
ADD CellServDB.dist /usr/vice/etc/
ADD CellServDB.local /usr/vice/etc/
ADD ThisCell /usr/vice/etc/
ADD CellServDB /etc/openafs/
ADD CellServDB.dist /etc/openafs/
ADD CellServDB.local /etc/openafs/
ADD ThisCell /etc/openafs/
RUN echo cern.ch > /usr/vice/etc/ThisCell
RUN echo cern.ch > /etc/openafs/ThisCell
RUN usermod -aG root condor

RUN mkdir -p /pool/condor
RUN chown -R condor:condor /pool
RUN mkdir -p /var/mnt
RUN ln -s /pool /var/mnt/pool 
